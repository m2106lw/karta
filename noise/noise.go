package noise

import "github.com/ojrac/opensimplex-go"

// Noise wraps simplexnoise.SimplexNoise
type Noise struct {
	opensimplex.Noise
}

// New generates a new Noise instance
/*
func New(seed int64) *Noise {
	return &Noise{simplexnoise.NewSimplexNoise(seed)}
}
*/

func New(seed int64) *Noise {
	return &Noise{opensimplex.New(seed)}
}
