module gitlab.com/m2106lw/karta

go 1.16

require (
	github.com/llgcode/draw2d v0.0.0-20210313082411-577c1ead272a
	github.com/ojrac/opensimplex-go v1.0.2
	github.com/pzsz/voronoi v0.0.0-20130609164533-4314be88c79f
)
